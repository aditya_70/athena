'use strict';

const socket = io();

//const outputYou = document.querySelector('.output-you');
//const outputBot = document.querySelector('.output-bot');

const SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition;
const recognition = new SpeechRecognition();

recognition.lang = 'en-US';
recognition.interimResults = false;
recognition.maxAlternatives = 1;

function inputText() {
  let search =  document.getElementById("inputText").value;
  document.getElementById("inputText").value = ""
  //outputYou.innerHTML = search;
  let conversationDiv = document.getElementById('conversationDiv')
  let paragraph = document.createElement('div')
  paragraph.classList.add('user-request')
  paragraph.innerHTML = 'you : ' + search
  conversationDiv.appendChild(paragraph)

  socket.emit('chat message', search);
  console.log(search);
 }

 function inputTextByEnterKey(e) {
   if (e.keyCode === 13)
   {
    let search =  document.getElementById("inputText").value;
    document.getElementById("inputText").value = ""
    //outputYou.innerHTML = search;
    let conversationDiv = document.getElementById('conversationDiv')
    let paragraph = document.createElement('div')
    paragraph.classList.add('user-request')
    paragraph.innerHTML = 'you : ' + search
    conversationDiv.appendChild(paragraph)
  
    socket.emit('chat message', search);
    console.log(search);
   }
   else
   {
     //console.log("not enter key pressed")
   }
 }
 

// document.querySelector('button').addEventListener('click', () => {
//   recognition.start();
// });

document.querySelector('#microphone').addEventListener('click', () => {
  recognition.start();
});

recognition.addEventListener('speechstart', () => {
  console.log('Speech has been detected.');
});

recognition.addEventListener('result', (e) => {
  console.log('Result has been detected.');

  let last = e.results.length - 1;
  let text = e.results[last][0].transcript;

  //outputYou.textContent = text;

  let conversationDiv = document.getElementById('conversationDiv')
  let paragraph = document.createElement('div')
  paragraph.classList.add('user-request')
  paragraph.innerHTML = 'you : ' + text
  conversationDiv.appendChild(paragraph)

  console.log('Confidence: ' + e.results[0][0].confidence);

  socket.emit('chat message', text);
});

recognition.addEventListener('speechend', () => {
  recognition.stop();
});

recognition.addEventListener('error', (e) => {
 // outputBot.textContent = 'Error: ' + e.error;
 let conversationDiv = document.getElementById('conversationDiv')
 let paragraph = document.createElement('div')
 paragraph.classList.add('server-response')
 paragraph.innerHTML = 'bot : ' + 'Error: ' + e.error
 conversationDiv.appendChild(paragraph)
});

function synthVoice(text) {
  const synth = window.speechSynthesis;
  const utterance = new SpeechSynthesisUtterance();
  utterance.text = text;
  synth.speak(utterance);
}

socket.on('bot reply', function(replyText) {
  synthVoice(replyText);

  if(replyText == '') replyText = 'No answer...';
 // outputBot.textContent = replyText;
 let conversationDiv = document.getElementById('conversationDiv')
 let paragraph = document.createElement('div')
 paragraph.classList.add('server-response')
 paragraph.innerHTML = 'bot : ' + replyText
 conversationDiv.appendChild(paragraph)
 
});
